#Jorge Luis Hidalgo
#Nelson Barrientos

def coverage(a, b):
    if a < 0 and b < 10:
        return True
    else:
        return False

def PruebaDecision():
    print(coverage (-2,-4))
    print(coverage (-2,4))
    print(coverage (2,-4))
    print(coverage (2,4))

def MCDC():
    a = 4
    b = -4
    assert a < 0 and b < 10, "Las condiciones no se cumplen"
    print(coverage (a,b))
    
def Multiple():
    a = -4
    b = -4
    test = False
    if a < 0:
        if b < 10:
            test = True
        else:
            test = False
    else:
        if b < 10:
            test = False
        else:
            test = False
    print(test)

PruebaDecision()
MCDC()
Multiple()
